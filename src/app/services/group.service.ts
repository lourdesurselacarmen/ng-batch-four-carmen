import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GroupList } from '../interfaces/user.interface';
import { Observable, map, catchError } from 'rxjs';
import { ResponseBase } from '../interfaces/response.interface';
import { RequestRegisterGroup, ResponseRegisterGroup } from '../interfaces/group.interface';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  private baseApi = 'http://localhost:8080'

  constructor(private httpClient: HttpClient) { }

  getlistGroup(): Observable<GroupList[]> {

    return this.httpClient.get<ResponseBase<GroupList[]>>(`${this.baseApi}/api/arisan-arisan-club/search-group`).pipe(
      map((val) => {
        return val.data;
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }

  registerGroup(totalMoney: number, payload: RequestRegisterGroup): Observable<ResponseRegisterGroup> {
    return this.httpClient.post<ResponseRegisterGroup>(this.baseApi + '/api/arisan-arisan-club/admin/create-group/' + totalMoney, payload)
  }
}
