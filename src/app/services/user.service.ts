import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, map } from 'rxjs';
import { UploadFoto, Userdata, UserList } from '../interfaces/user.interface';
import { ResponseBase } from '../interfaces/response.interface';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseApi = 'http://localhost:8080'

  private baseApingrok = 'https://1cfe-125-164-20-251.ap.ngrok.io'

  constructor(private httpClient: HttpClient) { }

  err:any

  getlistUser(): Observable<UserList[]> {

    // const token = localStorage.getItem('token')
    // console.log(token)

    // const headers = new HttpHeaders({
    //   'Authorization': `Bearer ${(token)}`,
    // })

    return this.httpClient.get<ResponseBase<UserList[]>>(`${this.baseApi}/api/arisan-arisan-club/userdata`).pipe(
      map((val) => {
        
        return val.data;
      }),
      catchError((err) => {
        console.log(err)
        throw err
      })
    )
  }

  // uploadPhoto(data: File): Observable<ResponseBase<UploadFoto>> {
  //   const file = new FormData
  //   file.append('file', data, data.name)
  //   return this.httpClient.post<ResponseBase<UploadFoto>>(`${this.baseApingrok}/users/uploadPhoto`, file)
  // }

  adduser(payload: any) {
    return this.httpClient.post<ResponseBase<any>>(`${this.baseApingrok}/register`, payload)
  }

  updatePassword(idUser: number, payload: Userdata): Observable<Userdata> {
      return this.httpClient.post<Userdata>(this.baseApi + '/api/arisan-arisan-club/userdata/ganti-password-sementara/' + idUser, payload)
  }
}
