import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {
  @Input() title = 'ng-batch-four-carmen';

  @Input() name = 'Lourdes Ursela Carmen'
  @Input() age = 22
  @Input() status = false
  //ini biar dinamis inputnya

  @Output() dataCallBack = new EventEmitter()
  //ngirim output dari child ke parent

  doClick() {
    this.dataCallBack.emit({Data: {title: 'Data List', name: this.name, age: this.age, status: this.status}})
    //button manggil function di html nanti fungsi ini bakal jalan
  }
}
