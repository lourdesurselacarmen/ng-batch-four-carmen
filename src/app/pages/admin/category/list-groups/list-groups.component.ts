import { Component, TemplateRef } from '@angular/core';
import { GroupList } from 'src/app/interfaces/user.interface';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { GroupService } from 'src/app/services/group.service';
import { AuthService } from 'src/app/services/auth.service';
import { Validators } from '@angular/forms';
import { switchMap, map, Subject, takeUntil } from 'rxjs';
import * as dayjs from 'dayjs';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-list-groups',
  templateUrl: './list-groups.component.html',
  styleUrls: ['./list-groups.component.scss']
})
export class ListGroupsComponent {
  modalRef!: BsModalRef
  groups: GroupList[] = []
  photo!: string
  photoFile!: File
  formRegisterGroup: FormGroup
  totalMoney!: number
  refresh = new Subject<void>()

  constructor(private groupService: GroupService, private modalService: BsModalService, private authService: AuthService, private formBuilder: FormBuilder) {
    this.loadData()
    this.formRegisterGroup = this.formBuilder.group({
      groupName: ['', [Validators.required]],
      passcode: ['', [Validators.required, Validators.minLength(8)]],
      createdDate: ['', [Validators.required]]
    })

    this.groupService.getlistGroup().subscribe(
      response => {
        console.log(response)
        this.groups = response
        //response ini untuk nampung dulu data yang di get
      }
    )

  }

  loadData() {
    this.groupService
      .getlistGroup()
      .pipe(takeUntil(this.refresh))
      .subscribe((response) => {
        console.log(response);
        this.groups = response;
      });
  }

  get errorControl() {
    return this.formRegisterGroup.controls
    //untuk handling error dri validators
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template)
  }

  doClick(money: number) {
    switch (money) {
      case 500000:
        this.totalMoney = 500000
        break
      case 1000000:
        this.totalMoney = 1000000
        break
      case 2000000:
        this.totalMoney = 2000000
        break
      case 3000000:
        this.totalMoney = 3000000
        break
      default:
        this.totalMoney = 0
    }
  }

  doAddGroup() {
    this.groupService.getlistGroup().pipe(
      switchMap((val) => {
        const payload = {
          groupName: this.formRegisterGroup.value.groupName,
          passcode:  this.formRegisterGroup.value.passcode,
          createdDate: dayjs(this.formRegisterGroup.value.createdDate).format('YYYY-MM-DD'),
        }
        return this.groupService.registerGroup(this.totalMoney, payload).pipe(
          map(val => val)
        )
      })
    ).subscribe(response => {
      Swal.fire({
        title: 'Success!',
        text: 'Anda berhasil menambahkan akun!',
        icon: 'success',
        confirmButtonText: 'OK'
      })
      console.log(response)
      this.loadData()

    })
  }
}
