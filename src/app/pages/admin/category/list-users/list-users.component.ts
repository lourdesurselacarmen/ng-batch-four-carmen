import { Component, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { switchMap } from 'rxjs';
import { UserList } from 'src/app/interfaces/user.interface';
import { AuthService } from 'src/app/services/auth.service';
import { UserService } from 'src/app/services/user.service';
import { map } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { Validators } from '@angular/forms';
import * as dayjs from 'dayjs';
import { RequestRegister } from 'src/app/interfaces/authInterface';
import { Userdata } from 'src/app/interfaces/user.interface';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent {
  modalRef!: BsModalRef
  users: UserList[] = []
  photo!: string
  photoFile!: File
  formRegister: FormGroup
  formUpdateUser: FormGroup
  userDetail!: Userdata

  constructor(private userService: UserService, private modalService: BsModalService, private authService: AuthService, private formBuilder: FormBuilder) {

    this.userService.getlistUser().subscribe(
      response => {
        console.log(response)
        this.users = response
        //response ini untuk nampung dulu data yang di get
      }
    )

    this.formRegister = this.formBuilder.group({
      username: ['', [Validators.required]],
      fullName: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required, Validators.minLength(8)]],
      dateOfBirth: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      repeatPass: ['', [Validators.required, Validators.minLength(8)]],
      admin: ['', [Validators.required]] 
    })

    this.formUpdateUser = this.formBuilder.group({
      idUser: [''],
      fullName: ['', [Validators.required]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      phoneNumber: ['', [Validators.required, Validators.minLength(8)]],
      dateOfBirth: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      admin: ['', [Validators.required]]
    })
  }

  get errorControl() {
    return this.formRegister.controls
    //untuk handling error dri regex
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template)
    
  }

  // showPreview(event: any) {
  //   if(event) {
  //     const file = event.target.files[0]
  //     //buat ambil gambarnya
  //     this.photoFile = file
  //     //nampung file
  //     const reader = new FileReader()
  //     //buat bacanya
  //     reader.onload = () => {
  //       this.photo = reader.result as string
  //     }
  //     reader.readAsDataURL(file)
  //   }
  // }

  // doAddUser() {
  //   this.userService.uploadPhoto(this.photoFile).pipe(
  //     switchMap((val) => {
  //       const payload = {
  //         fullName: this.formRegister.value.fullName,
  //         username: this.formRegister.value.username,
  //         phoneNumber: this.formRegister.value.phoneNumber,
  //         dateOfBirth: dayjs(this.formRegister.value.dateOfBirth).format('YYYY-MM-DD'),
  //         gender: this.formRegister.value.gender,
  //         password: this.formRegister.value.password,
  //         repeatPass: this.formRegister.value.repeatPass,
  //         // photo: val.data.image
  //       }
  //       return this.authService.register(payload).pipe(
  //         map(val => val)
  //       )
  //     })
  //   ).subscribe(response => console.log(response))
  // }

  doShowData(data: Userdata) {
    console.log(data)
    this.userDetail = data

    this.formUpdateUser.controls['fullName'].patchValue(data.fullName)
    this.formUpdateUser.controls['username'].patchValue(data.username)
    this.formUpdateUser.controls['password'].patchValue(data.password)
    this.formUpdateUser.controls['phoneNumber'].patchValue(data.phoneNumber)
    this.formUpdateUser.controls['dateOfBirth'].patchValue(data.dateOfBirth)
    this.formUpdateUser.controls['gender'].patchValue(data.gender)
    this.formUpdateUser.controls['admin'].patchValue(data.admin)
  }

  doChangePassword() {
    this.userService.getlistUser().pipe(
      switchMap((val) => {
        const payload = {
          idUser: this.formUpdateUser.value.idUser,
          fullName: this.formUpdateUser.value.fullName,
          username: this.formUpdateUser.value.username,
          password: this.formUpdateUser.value.password,
          phoneNumber: this.formUpdateUser.value.phoneNumber,
          dateOfBirth: dayjs(this.formUpdateUser.value.dateOfBirth).format('YYYY-MM-DD'),
          gender: this.formUpdateUser.value.gender,
          admin: this.formUpdateUser.value.admin
        }
        return this.userService.updatePassword(this.userDetail.idUser, payload).pipe(
          map(val => val)
        )
      })
    ).subscribe(response => {
      Swal.fire({
        title: 'Success!',
        text: 'Anda berhasil menambahkan grup',
        icon: 'success',
        confirmButtonText: 'OK'
      })
      console.log(response)
    })
  }
}
