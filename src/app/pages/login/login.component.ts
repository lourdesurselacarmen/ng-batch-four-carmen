import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ResponseLogin } from 'src/app/interfaces/authInterface';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  username = ''
  password = ''
  login = true

  formLogin: FormGroup

  constructor(private authService: AuthService, private router: Router, private formBuilder: FormBuilder) {
    this.formLogin = this.formBuilder.group({
      username: ['', [Validators.required, Validators.minLength(1)]],
      //menunjukkan bahwa username ini valuenya harus ada
      password: ['', [Validators.required, Validators.minLength(8)]]
      //disini bisa nambah regex juga
    })
  }

  get errorControl() {
    return this.formLogin.controls
    //untuk handling error dri regex
  }

  doLogin() {
    const payload = {
      username: this.formLogin.value.username,
      password: this.formLogin.value.password
    }

    this.authService.login(payload).subscribe(
      response => {
        if (response.data.role === "admin") {
          Swal.fire({
            title: 'Anda berhasil melakukan log in',
            text: 'Welcome',
            icon: 'success',
            confirmButtonText: 'OK'
          })
          // alert("Anda telah berhasil melakukan log in")
          //bisa alert dan di dalam kurung apa pesannya
          this.login = true
          console.log(response)
          localStorage.setItem('token', response.data.token)
          //untuk akses token dan diambil
          this.router.navigate(['/admin/dashboard'])
          //mau diarahin ke hlm mana
          //kalo mau ada role, balikannya jangan cuma token tapi pke role juga
        } else {
          Swal.fire({
            title: 'Login tidak berhasil, anda bukan merupakan seorang admin',
            text: 'Jika anda seorang user, silahkan login di mobile apps',
            icon: 'error',
            confirmButtonText: 'OK'
          })
          // alert("Login tidak berhasil, anda bukan merupakan seorang admin. Jika anda seorang user silahkan log in di aplikasi smarthphonemu.")
          this.login = false
          console.log('Error, please try again')
        }
      }, error => {
        console.log(error)
        Swal.fire({
          title: 'Error!',
          text: 'Check again your username and password',
          icon: 'error',
          confirmButtonText: 'OK'
        })
        // alert(error.error.message)
      }

    )
  }
}
