import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import * as dayjs from 'dayjs'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
dayjs().format()
import Swal from 'sweetalert2'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  fullName = ''
  username = ''
  phoneNumber = ''
  dateOfBirth = ''
  gender = ''
  password = ''
  repeatPass = ''

  formRegister: FormGroup

  constructor(private authService: AuthService, private router: Router , private formBuilder: FormBuilder) {
    this.formRegister = this.formBuilder.group({
      username: ['', [Validators.required]],
      fullName: ['', [Validators.required]],
      phoneNumber: ['', [Validators.required, Validators.minLength(8)]],
      dateOfBirth: [''],
      gender: [''],
      password: ['', [Validators.required, Validators.minLength(8)]],
      repeatPass: ['', [Validators.required, Validators.minLength(8)]]
    })
  }

  get errorControl() {
    return this.formRegister.controls
    //untuk handling error dri regex
  }

  get confirmPass() {
    return this.formRegister.value.password == this.formRegister.value.repeatPass
  }

  doRegister() {

    const payload = {
      fullName: this.formRegister.value.fullName,
      username: this.formRegister.value.username,
      phoneNumber: this.formRegister.value.phoneNumber,
      dateOfBirth: dayjs(this.formRegister.value.dateOfBirth).format('YYYY-MM-DD'),
      gender: this.formRegister.value.gender,
      password: this.formRegister.value.password,
      repeatPass: this.formRegister.value.repeatPass
    }

    this.authService.register(payload).subscribe(
      response => {
        Swal.fire({
          title: 'Anda berhasil melakukan registrasi akun!',
          text: 'Silahkan lakukan log in',
          icon: 'success',
          confirmButtonText: 'OK'
        })
        // alert("Anda berhasil melakukan registrasi akun, silahkan lakukan log in")
        console.log(response)
        this.router.navigate(['/login'])
      }
    )
  }

}
