export interface RequestRegisterGroup {
    groupName: string
    createdDate: string
    passcode: string
}

export interface ResponseRegisterGroup {
    idGroup: number
    groupName: string
    createdDate: string
    totalMoney: number
    totalMember: number
}