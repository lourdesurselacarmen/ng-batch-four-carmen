export interface UserList {
    "idUser": number
    "fullName": string
    "username": string
    "phoneNumber": string
    "dateOfBirth": string
    "gender": string
    "wallet": number
    "admin": boolean
    // "photo": string
  }

  export interface Userdata {
    "idUser": number
    "fullName": string
    "username": string
    "password": string
    "phoneNumber": string
    "dateOfBirth": string
    "gender": string
    "admin": boolean
  }

  export interface UploadFoto {
    image: string
  }

  export interface GroupList {
    "idGroup": number
    "groupName": string
    "totalMember": number
    "createdDate": Date
    "totalMoney": number
  }
  