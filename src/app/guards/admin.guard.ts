import { Injectable } from '@angular/core';
import { CanLoad, Router } from '@angular/router';
import { filter, map, Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanLoad {

  constructor(private authService: AuthService, private router: Router) { }
  canLoad(): Observable<boolean> {
    return this.authService.isAuthenticated.pipe(
      filter((val) => val !== null),
      //filter data untuk yg diterima saat subscribe
      map((val) => {
        //map itu buat bikin satu fungsi baru
        if (val) {
          return true
        } else {
          this.router.navigate(['/login'])
          return false
        }
      }
      )
    )
  }

}
